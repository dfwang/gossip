#-*-encoding: utf-8 -*-
#
# @author cescgao

from django.db import models


class User(models.Model):
    
    nickname = models.CharField(max_length=128, db_index=True)
    email = models.CharField(max_length=64, blank=True)
    password = models.CharField(max_length=64, blank=True)
    picture_url = models.CharField(max_length=128, blank=True)
    last_login = models.DateTimeField(auto_now_add=True, null=True)
    gmt_create = models.DateTimeField(auto_now_add=True, null=True)
    gmt_modify = models.DateTimeField(auto_now=True, null=True)

    def __unicode__(self):
        return "%s" % (self.nickname)

    class Meta:
        unique_together = (('nickname', ), ('email', ))


class Access(models.Model):
    user_id = models.IntegerField(db_index=True)
    token = models.CharField(max_length=128, db_index=True)
    gmt_create = models.DateTimeField(auto_now_add=True, null=True)
    gmt_modify = models.DateTimeField(auto_now=True, null=True)

    def __unicode(self):
        return unicode(self.user_id)

    class Meta:
        unique_together = (('user_id', ), ('token', ))
